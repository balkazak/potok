$(function() {

	// Custom JS
    var swiperResults = new Swiper('.swiper-results', {
        navigation: {
            nextEl: '.sbn-results',
            prevEl: '.sbp-results',
        },
    });
    var swiperSpeakers = new Swiper('.swiper-speakers', {
        slidesPerView: 5,
        spaceBetween: 70,
        loop: true,
        navigation: {
            nextEl: '.sbn-speakers',
            prevEl: '.sbp-speakers',
        },
    });


});
